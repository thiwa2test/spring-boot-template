package com.devhub.rest_test.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.devhub.rest_test.config.ObjMapper;
import com.devhub.rest_test.model.RequestObj;
import com.devhub.rest_test.model.ResponseObj;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/test")
public class TestController {
	
	ObjectMapper objMapper = new ObjectMapper();

	@PostMapping("/postRq")
	@ResponseBody
	public ResponseObj testPost(@RequestBody RequestObj rqObj) {
		ResponseObj response = new ResponseObj();
		
		try {
			System.out.println(ObjMapper.getMapper().writeValueAsString(rqObj));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Error occur while object to json convertion");
		}
		
		return response;
	}
}
