package com.devhub.rest_test.config;

import java.text.SimpleDateFormat;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjMapper {
	private static ObjectMapper objMapper = null;
	private static String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
	
	private static void createObjMapper() {
		objMapper = new ObjectMapper();
		objMapper.setDateFormat(new SimpleDateFormat(dateTimeFormat));
	}
	
	public static ObjectMapper getMapper() {
		if (objMapper == null)
			createObjMapper();
		return objMapper;
	}
}
